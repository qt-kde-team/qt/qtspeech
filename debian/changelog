qtspeech-opensource-src (5.15.15-2) unstable; urgency=medium

  * Upload to unstable.

 -- Dmitry Shachnev <mitya57@debian.org>  Fri, 25 Oct 2024 12:46:27 +0300

qtspeech-opensource-src (5.15.15-1) experimental; urgency=medium

  * New upstream release.
  * Bump Qt build-dependencies to 5.15.15.
  * Bump Standards-Version to 4.7.0, no changes needed.

 -- Dmitry Shachnev <mitya57@debian.org>  Sun, 08 Sep 2024 12:26:33 +0300

qtspeech-opensource-src (5.15.13-2) unstable; urgency=medium

  [ Lisandro Damián Nicanor Pérez Meyer ]
  * Remove myself from Uploaders.

  [ Dmitry Shachnev ]
  * Upload to unstable.

 -- Dmitry Shachnev <mitya57@debian.org>  Tue, 21 May 2024 18:51:51 +0300

qtspeech-opensource-src (5.15.13-1) experimental; urgency=medium

  * New upstream release.
  * Bump Qt build-dependencies to 5.15.13.

 -- Dmitry Shachnev <mitya57@debian.org>  Wed, 13 Mar 2024 20:16:02 +0300

qtspeech-opensource-src (5.15.12-1) experimental; urgency=medium

  * New upstream release.
  * Bump Qt build-dependencies to 5.15.12.

 -- Dmitry Shachnev <mitya57@debian.org>  Sun, 24 Dec 2023 20:09:14 +0300

qtspeech-opensource-src (5.15.10-2) unstable; urgency=medium

  * Upload to unstable.

 -- Dmitry Shachnev <mitya57@debian.org>  Sat, 08 Jul 2023 19:18:00 +0300

qtspeech-opensource-src (5.15.10-1) experimental; urgency=medium

  * New upstream release.
  * Bump Qt build-dependencies to 5.15.10.

 -- Dmitry Shachnev <mitya57@debian.org>  Mon, 12 Jun 2023 00:06:46 +0300

qtspeech-opensource-src (5.15.9-1) experimental; urgency=medium

  * New upstream release.
  * Bump Qt build-dependencies to 5.15.9.

 -- Dmitry Shachnev <mitya57@debian.org>  Sun, 23 Apr 2023 21:09:33 +0300

qtspeech-opensource-src (5.15.8-2) unstable; urgency=medium

  * Upload to unstable.

 -- Dmitry Shachnev <mitya57@debian.org>  Fri, 13 Jan 2023 12:02:09 +0400

qtspeech-opensource-src (5.15.8-1) experimental; urgency=medium

  * New upstream release.
  * Bump Qt build-dependencies to 5.15.8.
  * Bump Standards-Version to 4.6.2, no changes needed.

 -- Dmitry Shachnev <mitya57@debian.org>  Sat, 07 Jan 2023 17:59:33 +0400

qtspeech-opensource-src (5.15.7-2) unstable; urgency=medium

  * Upload to unstable.

 -- Dmitry Shachnev <mitya57@debian.org>  Sat, 17 Dec 2022 18:20:33 +0300

qtspeech-opensource-src (5.15.7-1) experimental; urgency=medium

  * New upstream release.
  * Bump Qt build-dependencies to 5.15.7.

 -- Dmitry Shachnev <mitya57@debian.org>  Wed, 07 Dec 2022 13:44:44 +0300

qtspeech-opensource-src (5.15.6-2) unstable; urgency=medium

  * Upload to unstable.

 -- Dmitry Shachnev <mitya57@debian.org>  Thu, 29 Sep 2022 11:55:49 +0300

qtspeech-opensource-src (5.15.6-1) experimental; urgency=medium

  * New upstream release.
  * Bump Qt build-dependencies to 5.15.6.

 -- Dmitry Shachnev <mitya57@debian.org>  Tue, 13 Sep 2022 13:53:23 +0300

qtspeech-opensource-src (5.15.5-1) experimental; urgency=medium

  * New upstream release.
  * Bump Qt build-dependencies to 5.15.5.
  * Use symver directive to catch all private symbols at once.

 -- Dmitry Shachnev <mitya57@debian.org>  Sat, 30 Jul 2022 22:01:19 +0300

qtspeech-opensource-src (5.15.4-2) unstable; urgency=medium

  * Upload to unstable.

 -- Dmitry Shachnev <mitya57@debian.org>  Mon, 13 Jun 2022 21:36:53 +0300

qtspeech-opensource-src (5.15.4-1) experimental; urgency=medium

  * New upstream release.
  * Bump Qt build-dependencies to 5.15.4.
  * Bump Standards-Version to 4.6.1, no changes needed.

 -- Dmitry Shachnev <mitya57@debian.org>  Sat, 14 May 2022 12:57:04 +0300

qtspeech-opensource-src (5.15.3-1) experimental; urgency=medium

  * New upstream release.
  * Update debian/watch.
  * Bump Qt build-dependencies to 5.15.3.
  * Update email address for Patrick Franz.
  * Bump Standards-Version to 4.6.0, no changes needed.
  * Do not install private headers.

 -- Dmitry Shachnev <mitya57@debian.org>  Tue, 08 Mar 2022 18:42:45 +0300

qtspeech-opensource-src (5.15.2-2) unstable; urgency=medium

  * Bump Standards-Version to 4.5.1, no changes needed.
  * Upload to unstable.

 -- Dmitry Shachnev <mitya57@debian.org>  Fri, 11 Dec 2020 11:32:22 +0300

qtspeech-opensource-src (5.15.2-1) experimental; urgency=medium

  [ Debian Janitor ]
  * Apply multi-arch hints.
    + qtspeech5-doc, qtspeech5-doc-html: Add Multi-Arch: foreign.

  [ Dmitry Shachnev ]
  * New upstream release.
  * Bump Qt build-dependencies to 5.15.2.
  * Build-depend only on the needed documentation tools, not on the
    large qttools5-dev-tools package.

 -- Dmitry Shachnev <mitya57@debian.org>  Mon, 23 Nov 2020 21:41:12 +0300

qtspeech-opensource-src (5.15.1-2) unstable; urgency=medium

  * Upload to unstable.

 -- Dmitry Shachnev <mitya57@debian.org>  Wed, 28 Oct 2020 21:54:12 +0300

qtspeech-opensource-src (5.15.1-1) experimental; urgency=medium

  [ Dmitry Shachnev ]
  * Recommend the plugins instead of depending on them (closes: #963848).
  * Stop installing cmake files for the plugins.

  [ Patrick Franz ]
  * New upstream release (5.15.1).
  * Bump Qt build-dependencies to 5.15.1.
  * Bump debhelper-compatibility to 13:
    - Do not override dh_missing as --fail-missing is now the default.
    - Use the new substitution variables from debhelper 13 in install files.
  * Update libqt5texttospeech5.symbols from the current build log.
  * Add hardening=+all build flag.

 -- Lisandro Damián Nicanor Pérez Meyer <lisandro@debian.org>  Wed, 16 Sep 2020 14:38:20 -0300

qtspeech-opensource-src (5.14.2-2) unstable; urgency=medium

  * Make libqt5texttospeech5 depend on qtspeech5-speechd-plugin |
    qtspeech5-flite-plugin (closes: #958747).
  * Upload to unstable.

 -- Dmitry Shachnev <mitya57@debian.org>  Wed, 24 Jun 2020 12:59:43 +0300

qtspeech-opensource-src (5.14.2-1) experimental; urgency=medium

  [ Patrick Franz ]
  * New upstream release (closes: #959664).
  * Bump Standards-Version to 4.5.0, no changes needed.
  * Bump debhelper-compatibility to 12:
    - Switch debhelper build dependency to debhelper-compat 12.
    - Remove debian/compat.
  * Update the Uploaders-field.
  * Bump Qt build-dependencies to 5.14.2.
  * Add Rules-Requires-Root field.
  * Update debian/copyright.
  * Update debian/libqt5texttospeech5.symbols from the current build
    log.

  [ Dmitry Shachnev ]
  * Run tests with QTEST_ENVIRONMENT=ubuntu, to make say_hello test
    blacklisted again after upstream BLACKLIST changes.
  * Remove build paths from .prl files for reproducibility.

 -- Patrick Franz <patfra71@gmail.com>  Sun, 03 May 2020 18:40:47 +0200

qtspeech-opensource-src (5.12.5-1) unstable; urgency=medium

  * New upstream release.
  * Bump Qt build-dependencies to 5.12.5.
  * Bump Standards-Version to 4.4.1, no changes needed.
  * Upload to unstable.

 -- Dmitry Shachnev <mitya57@debian.org>  Mon, 21 Oct 2019 19:18:22 +0300

qtspeech-opensource-src (5.12.4-1) experimental; urgency=medium

  [ Scarlett Moore ]
  * Update packaging to use doc-base as per policy 9.10.

  [ Dmitry Shachnev ]
  * New upstream release.
  * Bump Qt build-dependencies to 5.12.4.
  * Simplify debian/rules using debian/not-installed file.

 -- Dmitry Shachnev <mitya57@debian.org>  Tue, 02 Jul 2019 19:50:41 +0300

qtspeech-opensource-src (5.12.2-1) experimental; urgency=medium

  * New upstream release.
  * Bump Qt build-dependencies to 5.12.2.
  * Drop flite_volume.diff, included in the new release.
  * Add Qt_5.12 version tag to debian/libqt5texttospeech5.symbols.
  * Add Build-Depends-Package field to debian/libqt5texttospeech5.symbols.

 -- Dmitry Shachnev <mitya57@debian.org>  Tue, 26 Mar 2019 15:24:12 +0300

qtspeech-opensource-src (5.11.3-3) unstable; urgency=medium

  * Backport upstream change to fix volume for flite (closes: #924799).

 -- Dmitry Shachnev <mitya57@debian.org>  Mon, 25 Mar 2019 23:57:20 +0300

qtspeech-opensource-src (5.11.3-2) unstable; urgency=medium

  [ Simon Quigley ]
  * Change my email to tsimonq2@debian.org now that I am a Debian Developer.
  * Bump Standards-version to 4.3.0, no changes needed.

  [ Lisandro Damián Nicanor Pérez Meyer ]
  * Upload to unstable.

 -- Lisandro Damián Nicanor Pérez Meyer <lisandro@debian.org>  Wed, 26 Dec 2018 16:55:11 -0300

qtspeech-opensource-src (5.11.3-1) experimental; urgency=medium

  [ Dmitry Shachnev ]
  * Make tests fatal on s390x again, bug #910792 has been fixed.

  [ Lisandro Damián Nicanor Pérez Meyer ]
  * New upstream release.
    - Bump Qt build dependencies.

 -- Lisandro Damián Nicanor Pérez Meyer <lisandro@debian.org>  Sat, 22 Dec 2018 15:28:22 -0300

qtspeech-opensource-src (5.11.2-2) unstable; urgency=medium

  * Make tests non-fatal on s390x because of bug #910792.
  * Upload to unstable.

 -- Dmitry Shachnev <mitya57@debian.org>  Tue, 16 Oct 2018 19:03:01 +0300

qtspeech-opensource-src (5.11.2-1) experimental; urgency=medium

  [ Lisandro Damián Nicanor Pérez Meyer ]
  * New upstream release.
    - Bump Qt build dependencies.

  [ Dmitry Shachnev ]
  * Update the command to run tests:
    - Xvfb is not needed in this module, use QT_QPA_PLATFORM=minimal.
    - Set QT_HASH_SEED=0 to make the plugin loader choose flite plugin.
  * Use dh_missing instead of deprecated dh_install --fail-missing.
  * Bump years in debian/copyright.
  * Bump Standards-Version to 4.2.1, no changes needed.

 -- Dmitry Shachnev <mitya57@debian.org>  Wed, 10 Oct 2018 13:53:58 +0300

qtspeech-opensource-src (5.11.1-2) unstable; urgency=medium

  * Upload to Sid.

 -- Simon Quigley <tsimonq2@ubuntu.com>  Wed, 25 Jul 2018 04:49:32 -0500

qtspeech-opensource-src (5.11.1-1) experimental; urgency=medium

  * New upstream release.
  * Bump Standards-version to 4.1.4, no changes needed.
  * Bump build dependencies to 5.11.1.
  * Update symbols from amd64 build logs.
  * Remove --parallel from debian/rules; it's already the default.

 -- Simon Quigley <tsimonq2@ubuntu.com>  Sat, 30 Jun 2018 17:14:41 -0500

qtspeech-opensource-src (5.10.1-2) unstable; urgency=medium

  * Release to unstable.

 -- Lisandro Damián Nicanor Pérez Meyer <lisandro@debian.org>  Sat, 07 Apr 2018 18:04:15 -0300

qtspeech-opensource-src (5.10.1-1) experimental; urgency=medium

  * New upstream release.
    - Bump Qt build dependencies.
  * Update watch file to follow upstream's new naming convention.
  * Support the nodoc build profile.
  * Add myself to Uploaders.
  * Switch Vcs-[Git Browser] to salsa.debian.org.
  * Switch to compat 11.
  * Do not use qmake directly anymore. This with compat 11 ensures the
    submodule can be crossbuilt.
    - Build and run tests.
  * Update symbols files with current build log.
  * Add libasound2-dev and libspeechd-dev as build dependencies, required to
    add flite_alsa and speechd support.
    - Add new binary package qtspeech5-speechd-plugin.
    - Add plugin's cmake file to the dev package. I'm not really sure this is
      needed.
  * Bump Standards-Version to 4.1.3.
    - Switch priorities "extra" to "optional".
  * Add myself to debian/copyright.

 -- Lisandro Damián Nicanor Pérez Meyer <lisandro@debian.org>  Thu, 08 Mar 2018 11:43:04 -0300

qtspeech-opensource-src (5.9.1-1) experimental; urgency=medium

  * New upstream release.
  * Bump Qt build-dependencies to 5.9.1.
  * Bump Standards-Version to 4.0.0, no changes needed.

 -- Dmitry Shachnev <mitya57@debian.org>  Mon, 17 Jul 2017 21:04:27 +0300

qtspeech-opensource-src (5.9.0-1) experimental; urgency=medium

  * New upstream release.

 -- Simon Quigley <tsimonq2@ubuntu.com>  Sun, 11 Jun 2017 14:57:36 -0500

qtspeech-opensource-src (5.8.0-1) experimental; urgency=medium

  * New upstream stable release.
  * Make the -dev package depend on libqt5texttospeech5 (LP: #1656535).
  * Bump Qt build-dependencies to 5.7.1.
  * Update Vcs-Browser and Homepage links.
  * Update debian/watch to track stable releases.

 -- Dmitry Shachnev <mitya57@debian.org>  Tue, 24 Jan 2017 13:09:57 +0300

qtspeech-opensource-src (5.8.0~alpha-1) experimental; urgency=medium

  * Initial package (Closes: #837293)
  * Team upload

 -- Simon Quigley <tsimonq2@ubuntu.com>  Sat, 10 Sep 2016 05:04:29 -0500
